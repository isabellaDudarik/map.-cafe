import java.util.HashMap;
import java.util.Map;

public class Generate {
    public static Map<String, Float> getMenu() {
        Map<String, Float> menu = new HashMap<>();
        menu.put("pizza", 239.5f);
        menu.put("soup", 45.5f);
        menu.put("dessert", 140.2f);
        menu.put("tea", 30.2f);
        menu.put("coffee", 40.2f);
        menu.put("milkshake", 70.6f);
        menu.put("burger", 120.3f);
        return menu;
    }

}
