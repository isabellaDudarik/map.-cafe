import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
//        System.out.println("Enter the dish");
//        String dish = scan.nextLine();
//
//        System.out.println("Enter the next dish");
//        String dish2 = scan.nextLine();


        Map<String, Float> menu = Generate.getMenu();
//        float summ = menu.get(dish) + menu.get(dish2);
//        System.out.println(String.format("Your bill is: %.2f $", summ));

        float summ = 0f;
//        System.out.println("Enter the number of dishes:");
//        int number = Integer.parseInt(scan.nextLine());
//
//
//        for (int i = 1; i <= number; i++) {
//            System.out.println("Enter the dish:");
//            String dish = scan.nextLine();
//            summ = summ + menu.get(dish);
//        }
//        System.out.println(String.format("Your bill is: %.2f $", summ));


        for (int i = 0; ; i++) {
            System.out.println("Enter the dish:");
            String dish = scan.nextLine();
            if (!dish.equalsIgnoreCase("exit")) {
                summ = summ + menu.get(dish);
            } else {
                System.out.println(String.format("Your bill is: %.2f $", summ));
                break;
            }
        }
    }
}
